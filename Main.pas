unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, StdCtrls, ZAbstractRODataset,
  ZAbstractDataset, ZDataset, ZConnection, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ExtCtrls, Registry, Menus,
  cxGridCustomPopupMenu, cxGridPopupMenu, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSCore, frxClass, frxDBSet, frxExportCSV, cxPC, ComCtrls, ShlObj,
  cxShellCommon, cxContainer, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxShellComboBox, ExtDlgs;

type
  TfrmMain = class(TForm)
    conSQL: TZConnection;
    qrySQL: TZQuery;
    Panel1: TPanel;
    edtUser: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    edtHost: TEdit;
    Label3: TLabel;
    edtPass: TEdit;
    Label4: TLabel;
    edtDB: TEdit;
    btnConnect: TButton;
    cbxProtocol: TComboBox;
    btnExec: TButton;
    dtsSQL: TDataSource;
    popSQLGrid: TcxGridPopupMenu;
    popSQL: TPopupMenu;
    mniExecuteSQL: TMenuItem;
    mniConnect: TMenuItem;
    frxCSVExport1: TfrxCSVExport;
    frxDBDataset1: TfrxDBDataset;
    frxReport1: TfrxReport;
    PopupMenu1: TPopupMenu;
    mniExportCsv: TMenuItem;
    pcMain: TcxPageControl;
    tabSQLExplorer: TcxTabSheet;
    tabBatchExport: TcxTabSheet;
    memSQL: TMemo;
    Splitter1: TSplitter;
    grdSQL: TcxGrid;
    tvSQL: TcxGridDBTableView;
    grdSQLLevel1: TcxGridLevel;
    memExport: TMemo;
    Label5: TLabel;
    Panel2: TPanel;
    edtExportPath: TcxShellComboBox;
    Label6: TLabel;
    diaSaveToFile: TSaveTextFileDialog;
    Splitter2: TSplitter;
    qryExport: TZQuery;
    procedure btnConnectClick(Sender: TObject);
    procedure btnExecClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mniExportCsvClick(Sender: TObject);
  private
    procedure ExportCsv(PsFileName: string);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

procedure TfrmMain.ExportCsv(PsFileName: string);
var
  i: integer;
  slAux: TStringList;
  sLine, sStrDelim: string;
begin
    Screen.Cursor:= crHourGlass;
    slAux:= TStringList.Create;
    try
      for i:=0 to qrySQL.FieldList.Count -1 do
      begin
        sLine := sLine + qrySQL.FieldList[i].FieldName;
        if i < qrySQL.FieldList.Count -1 then
          sLine := sLine + ';'; // �ltimo valor n�o tem separador. Conferido: CSV � ;
      end;
      slAux.Add(sLine);

      qrySQL.DisableControls;
      try
        qrySQL.First;
        while not qrySQL.Eof do
        begin
          sLine:= '';
          for i:=0 to qrySQL.FieldList.Count -1 do
          begin
            if qrySQL.Fields[i].DataType in [ftUnknown, ftString, ftWord, ftBoolean, ftBytes,
                                              ftVarBytes, ftBlob, ftMemo, ftGraphic, ftFmtMemo,
                                              ftParadoxOle, ftDBaseOle, ftFixedChar, ftWideString, ftADT,
                                              ftArray, ftReference, ftDataSet, ftOraBlob, ftOraClob,
                                              ftVariant, ftInterface, ftIDispatch, ftGuid, ftTimeStamp,
                                              ftDate, ftDateTime, ftFMTBcd] then
              sStrDelim := '"'
            else
              sStrDelim := '';
            sLine := sLine + Format('%s%s%s', [sStrDelim, qrySQL.FieldList[i].AsString, sStrDelim]);
            if i < qrySQL.FieldList.Count -1 then
              sLine := sLine + ';'; // �ltimo valor n�o tem separador. Conferido: CSV � ;
          end; // for
          slAux.Add(sLine);
          qrySQL.Next;
        end; // while
      finally
        qrySQL.EnableControls;
      end;
      slAux.SaveToFile(PsFileName);
    finally
      slAux.Free;
      Screen.Cursor:= crDefault;
    end;
end;


// -----

procedure TfrmMain.btnConnectClick(Sender: TObject);
begin
  if conSQL.Connected then
    conSQL.Disconnect;
  conSQL.HostName := edtHost.Text;
  conSQL.Protocol := cbxProtocol.Text;
  conSQL.User     := edtUser.Text;
  conSQL.Password := edtPass.Text;
  conSQL.Database := edtDB.Text;
  conSQL.Catalog  := edtDB.Text;
  conSQL.Connect;

  btnExec.Enabled:= conSQL.Connected;
end;

procedure TfrmMain.btnExecClick(Sender: TObject);
var
  i: integer;
begin
  if pcMain.ActivePageIndex = 0 then
  begin
    // SQL Explorer
    if qrySQL.Active then
      qrySQL.Close;
    tvSQL.ClearItems;

    if memSQL.SelLength > 0 then
      qrySQL.SQL.Text:= memSQL.SelText
    else
      qrySQL.SQL.Text:= memSQL.Lines.Text;

    if pos('select', AnsiLowerCase(trim(qrySQL.SQL.Text))) = 1 then
      qrySQL.Open
    else
      qrySQL.ExecSQL;
    tvSQL.DataController.CreateAllItems;
  end
  else
  begin
    // Table Export
    // Tem que executar um select * pra cada tabela/view e salvar na pasta indicada um .csv com o nome da tabela
    for i:= 0 to memExport.Lines.Count - 1 do
    begin
      if qrySQL.Active then
        qrySQL.Close;

      qrySQL.SQL.Text := 'SELECT * from ' + memExport.Lines[i];
      qrySQL.Open;

      // Caminho + nome da tabela ou view
      ExportCsv(edtExportPath.Path + '\' + memExport.Lines[i] + '.csv');
    end; // for
    MessageDlg('Data exported succesfully!', mtInformation, [mbOK], 0);      
  end;
end;

procedure TfrmMain.FormShow(Sender: TObject);
var
  RG: TRegistry;
begin
  RG := TRegistry.Create;
  try
    RG.RootKey := HKEY_CURRENT_USER;
    if RG.OpenKey('\SOFTWARE\B4IT\FriendlySQL', True) then
    begin
      if RG.ValueExists('Protocol') then
        cbxProtocol.ItemIndex:= cbxProtocol.Items.IndexOf(RG.ReadString('Protocol'));
      if RG.ValueExists('Host') then
        edtHost.Text:= RG.ReadString('Host');
      if RG.ValueExists('User') then
        edtUser.Text:= RG.ReadString('User');
      if RG.ValueExists('Password') then
        edtPass.Text:= RG.ReadString('Password');
      if RG.ValueExists('DB') then
        edtDB.Text:= RG.ReadString('DB');
      if RG.ValueExists('LastSQL') then
        memSQL.Lines.Text:= RG.ReadString('LastSQL');
      if RG.ValueExists('LastExport') then
        memExport.Lines.Text:= RG.ReadString('LastExport');
      if RG.ValueExists('LastExportPath') then
        edtExportPath.Path := RG.ReadString('LastExportPath');
      if RG.ValueExists('LastFocusedTab') then
        pcMain.ActivePageIndex := RG.ReadInteger('LastFocusedTab');

      RG.CloseKey;
    end; //if
  finally
    RG.Free;
  end;
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
var
  RG: TRegistry;
begin
  RG := TRegistry.Create;
  try
    RG.RootKey := HKEY_CURRENT_USER;
    if RG.OpenKey('\SOFTWARE\B4IT\FriendlySQL', True) then
    begin
        RG.WriteString('Protocol',    cbxProtocol.Text);
        RG.WriteString('Host',        edtHost.Text);
        RG.WriteString('User',        edtUser.Text);
        RG.WriteString('Password',    edtPass.Text);
        RG.WriteString('DB',          edtDB.Text);
        RG.WriteString('LastSQL',     memSQL.Lines.Text);
        RG.WriteString('LastExport',  memExport.Lines.Text);
        RG.WriteString('LastExportPath', edtExportPath.Path);
        RG.WriteInteger('LastFocusedTab', pcMain.ActivePageIndex);
    end;
  finally
    RG.Free;
  end;
end;

procedure TfrmMain.mniExportCsvClick(Sender: TObject);
var
  i: integer;
  slAux: TStringList;
  sLine, sStrDelim: string;
  sFileName: string;
begin
  if diaSaveToFile.Execute then
  begin
    sFileName := diaSaveToFile.FileName;
    ExportCsv(sFileName);

    {
    slAux:= TStringList.Create;
    try
      for i:=0 to qrySQL.FieldList.Count -1 do
      begin
        sLine := sLine + qrySQL.FieldList[i].FieldName;
        if i < qrySQL.FieldList.Count -1 then
          sLine := sLine + ';'; // �ltimo valor n�o tem separador. Conferido: CSV � ;
      end;
      slAux.Add(sLine);

      qrySQL.First;
      while not qrySQL.Eof do
      begin
        sLine:= '';
        for i:=0 to qrySQL.FieldList.Count -1 do
        begin
          if qrySQL.Fields[i].DataType in [ftUnknown, ftString, ftWord, ftBoolean, ftBytes,
                                            ftVarBytes, ftBlob, ftMemo, ftGraphic, ftFmtMemo,
                                            ftParadoxOle, ftDBaseOle, ftFixedChar, ftWideString, ftADT,
                                            ftArray, ftReference, ftDataSet, ftOraBlob, ftOraClob,
                                            ftVariant, ftInterface, ftIDispatch, ftGuid, ftTimeStamp,
                                            ftFMTBcd] then
            sStrDelim := '"'
          else
            sStrDelim := '';
          sLine := sLine + Format('%s%s%s;', [sStrDelim, qrySQL.FieldList[i].AsString, sStrDelim]);
        end; // for
        slAux.Add(sLine);
        qrySQL.Next;
      end; // while
      slAux.SaveToFile(sFileName);
    }
      WinExec(PChar('notepad.exe ' + sFileName), SW_NORMAL);
    {
    finally
      slAux.Free;
    end;
    }
  end;
end;



end.

