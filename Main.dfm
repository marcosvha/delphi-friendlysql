object frmMain: TfrmMain
  Left = 26
  Top = 33
  Caption = 'B4IT Friendly SQL Explorer'
  ClientHeight = 606
  ClientWidth = 973
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = popSQL
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 973
    Height = 36
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 284
      Top = 11
      Width = 22
      Height = 13
      Caption = 'User'
    end
    object Label2: TLabel
      Left = 140
      Top = 11
      Width = 22
      Height = 13
      Caption = 'Host'
    end
    object Label3: TLabel
      Left = 428
      Top = 11
      Width = 46
      Height = 13
      Caption = 'Password'
    end
    object Label4: TLabel
      Left = 604
      Top = 11
      Width = 46
      Height = 13
      Caption = 'Database'
    end
    object edtUser: TEdit
      Left = 312
      Top = 7
      Width = 100
      Height = 21
      TabOrder = 2
    end
    object edtHost: TEdit
      Left = 168
      Top = 7
      Width = 100
      Height = 21
      TabOrder = 1
    end
    object edtPass: TEdit
      Left = 480
      Top = 7
      Width = 100
      Height = 21
      PasswordChar = '*'
      TabOrder = 3
    end
    object edtDB: TEdit
      Left = 656
      Top = 7
      Width = 100
      Height = 21
      TabOrder = 4
    end
    object btnConnect: TButton
      Left = 760
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Co&nnect'
      Default = True
      TabOrder = 5
      OnClick = btnConnectClick
    end
    object cbxProtocol: TComboBox
      Left = 8
      Top = 7
      Width = 121
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      Items.Strings = (
        'ado'
        'firebird-1.0'
        'firebird-1.5'
        'interbase-5'
        'interbase-6'
        'mssql'
        'mysql'
        'mysql-3.20'
        'mysql-3.23'
        'mysql-4.0'
        'mysql-4.1'
        'oracle'
        'oracle-9i'
        'postgresql'
        'sqlite'
        'sybase')
    end
    object btnExec: TButton
      Left = 864
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Execute &SQL'
      Enabled = False
      TabOrder = 6
      OnClick = btnExecClick
    end
  end
  object pcMain: TcxPageControl
    Left = 0
    Top = 36
    Width = 973
    Height = 570
    ActivePage = tabBatchExport
    Align = alClient
    TabOrder = 1
    ClientRectBottom = 570
    ClientRectRight = 973
    ClientRectTop = 24
    object tabSQLExplorer: TcxTabSheet
      Caption = 'SQL Explorer'
      ImageIndex = 0
      object Splitter1: TSplitter
        Left = 0
        Top = 229
        Width = 973
        Height = 12
        Cursor = crVSplit
        Align = alTop
        Beveled = True
        ResizeStyle = rsLine
      end
      object memSQL: TMemo
        Left = 0
        Top = 0
        Width = 973
        Height = 229
        Align = alTop
        TabOrder = 0
      end
      object grdSQL: TcxGrid
        Left = 0
        Top = 241
        Width = 973
        Height = 305
        Align = alClient
        PopupMenu = PopupMenu1
        TabOrder = 1
        object tvSQL: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = dtsSQL
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
        end
        object grdSQLLevel1: TcxGridLevel
          GridView = tvSQL
        end
      end
    end
    object tabBatchExport: TcxTabSheet
      Caption = 'Tables Export'
      ImageIndex = 1
      object Label5: TLabel
        Left = 136
        Top = 0
        Width = 22
        Height = 13
        Caption = 'Host'
      end
      object Splitter2: TSplitter
        Left = 0
        Top = 229
        Width = 973
        Height = 12
        Cursor = crVSplit
        Align = alTop
        Beveled = True
        ResizeStyle = rsLine
      end
      object memExport: TMemo
        Left = 0
        Top = 0
        Width = 973
        Height = 229
        Align = alTop
        TabOrder = 0
      end
      object Panel2: TPanel
        Left = 0
        Top = 241
        Width = 973
        Height = 305
        Align = alClient
        TabOrder = 1
        object Label6: TLabel
          Left = 8
          Top = 9
          Width = 60
          Height = 13
          Caption = 'Export path: '
        end
        object edtExportPath: TcxShellComboBox
          Left = 168
          Top = 6
          Properties.Root.BrowseFolder = bfDrives
          Properties.Root.CustomPath = 'c:\'
          Properties.ShowFullPath = sfpAlways
          Properties.ViewOptions = [scvoShowFiles]
          TabOrder = 0
          Width = 338
        end
      end
    end
  end
  object conSQL: TZConnection
    Protocol = 'ado'
    Port = 0
    AutoCommit = True
    ReadOnly = False
    TransactIsolationLevel = tiNone
    Connected = False
    SQLHourGlass = False
    Left = 40
    Top = 80
  end
  object qrySQL: TZQuery
    Connection = conSQL
    CachedUpdates = False
    RequestLive = False
    ParamCheck = True
    Params = <>
    ShowRecordTypes = [usUnmodified, usModified, usInserted]
    UpdateMode = umUpdateChanged
    WhereMode = wmWhereKeyOnly
    Options = [doCalcDefaults]
    Left = 80
    Top = 80
  end
  object dtsSQL: TDataSource
    DataSet = qrySQL
    Left = 120
    Top = 80
  end
  object popSQLGrid: TcxGridPopupMenu
    PopupMenus = <
      item
        HitTypes = []
        Index = 0
        PopupMenu = PopupMenu1
      end
      item
        HitTypes = []
        Index = 1
        PopupMenu = popSQL
      end>
    AlwaysFireOnPopup = True
    Left = 328
    Top = 344
  end
  object popSQL: TPopupMenu
    Left = 160
    Top = 80
    object mniConnect: TMenuItem
      Caption = 'Connect'
      ShortCut = 16462
    end
    object mniExecuteSQL: TMenuItem
      Caption = 'Execute SQL'
      ShortCut = 16467
      OnClick = btnExecClick
    end
  end
  object frxCSVExport1: TfrxCSVExport
    UseFileCache = True
    ShowProgress = True
    Separator = ';'
    OEMCodepage = False
    Left = 296
    Top = 344
  end
  object frxDBDataset1: TfrxDBDataset
    UserName = 'frxDBDataset1'
    CloseDataSource = False
    DataSource = dtsSQL
    Left = 360
    Top = 344
  end
  object frxReport1: TfrxReport
    Version = '4.0.23'
    DataSet = frxDBDataset1
    DataSetName = 'frxDBDataset1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39771.516334270840000000
    ReportOptions.LastChange = 39771.516334270840000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 392
    Top = 344
    Datasets = <>
    Variables = <>
    Style = <>
  end
  object PopupMenu1: TPopupMenu
    Left = 192
    Top = 80
    object mniExportCsv: TMenuItem
      Caption = 'Export to CSV'
      OnClick = mniExportCsvClick
    end
  end
  object diaSaveToFile: TSaveTextFileDialog
    DefaultExt = '.csv'
    FileName = 'Export.csv'
    Title = 'Export results to file'
    Left = 264
    Top = 344
  end
  object qryExport: TZQuery
    Connection = conSQL
    CachedUpdates = False
    RequestLive = False
    ParamCheck = True
    Params = <>
    ShowRecordTypes = [usUnmodified, usModified, usInserted]
    UpdateMode = umUpdateChanged
    WhereMode = wmWhereKeyOnly
    Options = [doCalcDefaults]
    Left = 80
    Top = 112
  end
end
