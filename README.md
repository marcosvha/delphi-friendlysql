### Delphi Friendly SQL

Executa queries em bancos de dados e exibe o resultado em um TcxGrid (Developer Express).  
O cxGrid permite ordenação, filtro, e todas aquelas "alegorias".  

Permite exportar os dados para .CSV.  

Requer componentes ZeosDBO para acesso aos bancos de dados, que pode ser MySQL, SQL Server, Orable, Interbase, Postgres, SQLite e vários outros.  

A lista completa suportada pelo Zeos é esta:  
ado
firebird-1.0
firebird-1.5
interbase-5
interbase-6
mssql
mysql
mysql-3.20
mysql-3.23
mysql-4.0
mysql-4.1
oracle
oracle-9i
postgresql
sqlite
sybase
